`Table des matières`

[TOC]



# 1. Definition de constante et constantes en C

## 1.1. Définition de constante

Syntax:

const type constant_name;

Example de code:

\#include<stdio.h> 

main() 

```c
{
 const int SIDE = 10;
 int area; 
 area = SIDE*SIDE;
 printf("The area of the square with side: %d is: %d sq. units"  , SIDE, area); 
}
```

## 1.2. Utilisation de constantes

Mot clé "const" peut être mis avant ou après int, ex:

const int ex = 10;

ou 

int const ex = 10;

## 1.3. Types de constantes

1. Numeric Constants
   - Integer Constants
   - Real Constants
2. Character Constants
   - Single Character Constants
   - String Constants
   - Backslash Character Constants

Types de constantes integer:

1. Decimal Integer
2. Octal Integer
3. Hexadecimal Integer

Nombres réels

Constantes character

Constante String

Backslash character constant

​	Servant à faire des tabs, retour de chariot, etc.

| Constants | Meaning         |
| --------- | --------------- |
| \a        | beep sound      |
| \b        | backspace       |
| \f        | form feed       |
| \n        | new line        |
| \r        | carriage return |
| \t        | horizontal tab  |
| \v        | vertical tab    |
| \'        | single quote    |
| \"        | double quote    |
| \\        | backslash       |
| \0        | null            |

Constantes secondaires

- [Array](https://www.w3schools.in/c-tutorial/arrays/)
- [Pointer](https://www.w3schools.in/c-tutorial/pointers/)
- [Structure](https://www.w3schools.in/c-tutorial/structures/)
- [Union](https://www.w3schools.in/c-tutorial/unions/)
- Enum

# 2. Opérateurs en C

## 2.1. Types d'opérateurs en C

1. Arithmetic Operators

2. Relational Operators

3. Logical Operators

4. Assignment Operators

5. Increment and Decrement Operators

6. Conditional Operator

7. Bitwise Operators

8. Special Operators

   ### 2.1.1. Opérateurs arythmétiques

   ​	

   | Operator | Description    |
   | -------- | -------------- |
   | +        | Addition       |
   | -        | Subtraction    |
   | *        | Multiplication |
   | /        | Division       |
   | %        | Modulus        |

Exemple de programme d'addition:

\#include <stdio.h>

void main()

```c
{

int i=3,j=7,k;

/* Variables Defining and Assign values */ k=i+j; printf("sum of two numbers is %d\n", k);

 }
```

​		Incréments et décréments

| Operator | Description |
| -------- | ----------- |
| ++       | Increment   |
| −−       | Decrement   |

Exemple de code

```c
\#include <stdio.h>
//stdio.h is a header file used for input.output purpose.

void main()
{
//set a and b both equal to 5.    
int a=5, b=5;

//Print them and decrementing each time.
//Use postfix mode for a and prefix mode for b.
printf("\n%d %d",a--,--b);
printf("\n%d %d",a--,--b);
printf("\n%d %d",a--,--b);
printf("\n%d %d",a--,--b);
printf("\n%d %d",a--,--b);
}
```

Sortie de ce programme

```
5 4
4 3
3 2
2 1
1 0
```



## 2.2. Opérateurs relationnels

| Operator | Description              |
| -------- | ------------------------ |
| ==       | Is equal to              |
| !=       | Is not equal to          |
| >        | Greater than             |
| <        | Less than                |
| >=       | Greater than or equal to |
| <=       | Less than or equal to    |

## 2.3. Opérateurs logiques

| Operator | Description                                                  |
| -------- | ------------------------------------------------------------ |
| &&       | *And* operator. It performs logical conjunction of two expressions. (if both expressions evaluate to True, result is True. If either expression evaluates to False, the result is False) |
| \|\|     | *Or* operator. It performs a logical disjunction on two expressions. (if either or both expressions evaluate to True, the result is True) |
| !        | *Not* operator. It performs logical negation on an expression. |

## 2.4. Opérateurs bitwise

| Operator | Description                     |
| -------- | ------------------------------- |
| <<       | Binary Left Shift Operator      |
| >>       | Binary Right Shift Operator     |
| ~        | Binary Ones Complement Operator |
| &        | Binary AND Operator             |
| ^        | Binary XOR Operator             |
| \|       | Binary OR Operator              |

## 2.5. Assignment operators

| Operator | Description                     |
| -------- | ------------------------------- |
| =        | Assign                          |
| +=       | Increments then assign          |
| -=       | Decrements then assign          |
| *=       | Multiplies then assign          |
| /=       | Divides then assign             |
| %=       | Modulus then assign             |
| <<=      | Left shift and assign           |
| >>=      | Right shift and assign          |
| &=       | Bitwise AND assign              |
| ^=       | Bitwise exclusive OR and assign |
| \|=      | Bitwise inclusive OR and assign |

## 2.6. Opérateurs conditionnels

| Operator | Description            |
| -------- | ---------------------- |
| ? :      | Conditional Expression |

## 2.7. Opérateurs spéciaux

| Operator | Description                               |
| -------- | ----------------------------------------- |
| sizeof() | Returns the size of a memory location.    |
| &        | Returns the address of a memory location. |
| *        | Pointer to a variable.                    |

Exemple de programme montrant l'utilité de size of:

```c
#include <stdio.h>
void main()
{
      int i=10; /* Variables Defining and Assign values */      printf("integer: %d\n", sizeof(i));
}
```

Sortie du programme:

![c-sizeof-operator](https://www.w3schools.in/wp-content/uploads/2014/08/c-sizeof-operator.jpg)

# 3. Types de données

ANSI C donne trois types de données

1. **Primary(Built-in) Data Types**:
   *void*, *int*, *char*, *double* and *float*.
2. **Derived Data Types**:
   *Array*, *References*, and *Pointers*.
3. **User Defined Data Types**:
   *Structure*, *Union*, and *Enumeration*.

## 3.1. Types de données primaires

| Types                  | Description                                                  |
| ---------------------- | ------------------------------------------------------------ |
| void                   | As the name suggests, it holds no value and is generally used for specifying the type of function or what it returns. If the function has a void type, it means that the function will not return any value. |
| int                    | Used to denote an integer type.                              |
| char                   | Used to denote a character type.                             |
| float, double          | Used to denote a floating point type.                        |
| int *, float *, char * | Used to denote a pointer type.                               |

En C99 on a ajouté trois types de plus

- _Bool
- _Complex
- _Imaginary

Déclarations de variables primaires

```c
int    age;
char   letter;
float  height, width;
```

## 3.2. Types de données dérivées

| Data Types | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| Arrays     | Arrays are sequences of data items having homogeneous values. They have adjacent memory locations to store values. |
| References | Function pointers allow referencing functions with a particular signature. |
| Pointers   | These are powerful C features which are used to access the memory and deal with their addresses. |

## 3.3. Types de données définies par l'utilisateur

| Types de données | Description                                                  |
| ---------------- | ------------------------------------------------------------ |
| Structure        | It is a package of variables of different types under a single name. This is done to handle data efficiently. "struct" keyword is used to define a structure. |
| Union            | These allow storing various data types in the same memory location. Programmers can define a union with different members, but only a single member can contain a value at a given time. It is used for |
| Enum             | Enumeration is a special data type that consists of integral constants, and each of them is assigned with a specific name. "enum" keyword is used to define the enumerated data type. |

Exemple de code pour définir des variables:

```c
#include <stdio.h>
int main()
{
    int a = 4000; // positive integer data type
    float b = 5.2324; // float data type
    char c = 'Z'; // char data type
    long d = 41657; // long positive integer data type
    long e = -21556; // long -ve integer data type
    int f = -185; // -ve integer data type
    short g = 130; // short +ve integer data type
    short h = -130; // short -ve integer data type
    double i = 4.1234567890; // double float data type
    float j = -3.55; // float data type
}
```

Exemple de code retournant la taille des variables

```c
#include <stdio.h>
#include <limits.h>
int main()
{
    printf("Storage size for int is: %d \n", sizeof(int));
    printf("Storage size for char is: %d \n", sizeof(char));
    return 0; 
}
```

Sortie de ce programme

![c-data-types](https://www.w3schools.in/wp-content/uploads/2014/08/c-data-types.jpg)

# 4. Déclaration de variables en C

Syntax:

```
type variable_name;
```

ou

```
type variable_name, variable_name, variable_name;
```

## 4.1. Déclaration de variable et initalisation

Exemple:

```c
int    width, height=5;
char   letter='A';
float  age, area;
double d;

/* actual initialization */width = 10;
age = 26.5;
```

### 4.1.1. Assignation des variables

Exemple:

```c
int width = 60;
int age = 31;
```

Les règles des assignations de variables:

- A variable name can consist of Capital letters A-Z, lowercase letters a-z, digits 0-9, and the underscore character.
- The first character must be a letter or underscore.
- Blank spaces cannot be used in variable names.
- Special characters like #, $ are not allowed.
- C keywords cannot be used as variable names.
- Variable names are case sensitive.
- Values of the variables can be numeric or alphabetic.
- Variable type can be char, int, float, double, or void.

Exemple de code pour afficher une variable à l'écran:

```c
#include<stdio.h>

void main()
{
    /* c program to print value of a variable */    int age = 33;
    printf("I am %d years old.\n", age);
}
```

Sorti de ce programme:

```
I am 33 years old.
```

# 5. Type Casting

## 5.1. Sans type casting 

Exemple de code:

```c
#include <stdio.h>
main ()
{
    int a;
    a = 15/6;
    printf("%d",a);
}
```

Sorti de ce programme

```
In the above C program, 15/6 alone will produce integer value as 2.
```

![c-before-type-casting](https://www.w3schools.in/wp-content/uploads/2014/09/c-before-type-casting.jpg)

## 5.2. Avec du type casting

Exemple de code:

```c
#include <stdio.h>
main ()
{
    float a;
    a = (float) 15/6;
    printf("%f",a);
}
```

Sorti de ce programme

```
After type cast is done before division to retain float value 2.500000.
```

![c-after-type-casting](https://www.w3schools.in/wp-content/uploads/2014/09/c-after-type-casting.jpg)

# 6. Structure du code en C

Exemple de code:

```c
/* Author: www.w3schools.in
Date: 2018-04-28
Description:
Writes the words "Hello, World!" on the screen */  
#include<stdio.h>

int main()
{
    printf("Hello, World!\n");
    return 0;
}
```

ou de façon différente

```c
/* Author: www.w3schools.in
Date: 2013-11-15
Description:
Writes the words "Hello, World!" on the screen */
#include<stdio.h>
#include<conio.h> 

void main()
{
    printf("Hello, World!\n");
    return;
}
```

Sorti de ce programme

![C program](https://www.w3schools.in/wp-content/uploads/2014/08/c.jpg)

## 6.1. Différentes parties de programmes en C

| Parties           | Description                                                  |
| ----------------- | ------------------------------------------------------------ |
| /* Comments */    | Comments are a way of explaining what makes a program. The compiler ignores comments and used by others to understand the code.orThis is a comment block, which is ignored by the compiler. Comment can be used anywhere in the program to add info about the program or code block, which will be helpful for developers to understand the existing code in the future easily.Comments in C - Video TutorialTo understand "C Comments" in more depth, please watch this video tutorial. https://youtu.be/chC6mvWKA5I |
| #include<stdio.h> | stdio is standard for input/output, this allows us to use some commands which includes a file called stdio.h.orThis is a preprocessor command. That notifies the compiler to include the header file stdio.h in the program before compiling the source-code. |
| int/void main()   | int/void is a return value, which will be explained in a while. |
| main()            | The main() is the main function where program execution begins. Every C program must contain only one main function.orThis is the main function, which is the default entry point for every C program and the void in front of it indicates that it does not return a value. |
| Braces            | Two curly brackets "{...}" are used to group all statements.orCurly braces which shows how much the main() function has its scope. |
| printf()          | It is a function in C, which prints text on the screen.orThis is another pre-defined function of C which is used to be displayed text string in the screen. |
| return 0          | At the end of the main function returns value 0.             |

# 7. Entrées et sorties en C

Exemple de code:

```c
#include<stdio.h>

void main()
{
int a,b,c;
printf("Please enter any two numbers: \n");
scanf("%d %d", &a, &b);
c =  a + b;
printf("The addition of two number is: %d", c);
}
```

Sortie du programme:

```
Please enter any two numbers:
12
3
The addition of two number is:15
```

## 7.1. Gestion des entrées et sorties

La librairie `stdlib` sert a gérer les entrées et sortie

1. Standard Input (stdin)
2. Standard Output (stdout)

### 7.1.1. Lire des caractères en C

Syntax:

```
var_name = getchar();
```

Exemple de code:

```c
#include<stdio.h>

void main()
{
char title;
title = getchar();
}
```

Une autre fonction existe utilisant `getc` pour accomplir la même chose qui est utilisé pour accepter des charactères de l'entrée standard

Voici la syntaxe:

```
int getc(FILE *stream);
```

### 7.1.2. Écrire des caractères en C

Syntax:

```
putchar(var_name);
```

Exemple de code:

```c
#include<stdio.h>

void main()
{
char result = 'P';
putchar(result);
putchar('\n');
}
```

`putc` peut aussi être utilisé pour envoyé des caractères

syntax:

```
int putc(int c, FILE *stream);
```



### 7.1.3. Entrées formatées

Syntax:

```
scanf("control string", arg1, arg2, ..., argn);
```



Pour lire des integers la spécification de champ est:

```
%w sd
```

Exemple de code:

```c
#include<stdio.h>

void main()
{
int var1= 60;
int var2= 1234;
scanf("%2d %5d", &var1, &var2);
}
```

### 7.1.4. Lire et écrire des Strings en C

Syntax:

```
char *gets(char *str);
```

et

```
int puts(const char *str)
```



# 8. Format specifiers

Il y a 6 types d'identifiants en C débutant par le signe `%` 

| Format Specifier | Description                       |
| ---------------- | --------------------------------- |
| %d               | Integer Format Specifier          |
| %f               | Float Format Specifier            |
| %c               | Character Format Specifier        |
| %s               | String Format Specifier           |
| %u               | Unsigned Integer Format Specifier |
| %ld              | Long Int Format Specifier         |

## 8.1. Integer %d

Est utilisé pour représenter un Integer avec printf()

```c
printf("%d",<variable name>);
```

## 8.2. Float %f

Est utilisé pour représenter un float avec printf()

```c
printf("%f", <variable name>);
```

## 8.3. Character %c

```c
printf("%c",<variable name>);
```

## 8.4. String %s

```c
printf("%s",<variable name>);
```

## 8.5. Integer non-signé %u

```c
printf("%u",<variable name>);
```

## 8.6. Long integer %ld

```c
printf("%ld",<variable name>);
```

# 9. Prises de décisions en C

En règle général en programmation C, les lignes de code sont exécutées séquentiellement, à moins qu'un saut soit effectué (par exemple un if peut causer un tel sauf, ou une boucle peut faire répéter une partie de code) 

![Decision Making Flowchart](https://www.w3schools.in/wp-content/uploads/2014/07/c-if.png)

## 9.1. if

En voici la syntaxe

```c
if(test_expression)
{
    statement 1;
    statement 2;
    ...
}
```

Exemple d'un programme en C pour démontrer comment utiliser le "if"

```c
#include<stdio.h>

main()
{
  int a = 15, b = 20;

  if (b & gt; a) {  
    printf("b is greater");
  }
}
```

La sortie de ce programme:

![c-decision-making-if-example1](https://www.w3schools.in/wp-content/uploads/2014/08/c-decision-making-if-example1.jpg)

Et voici un deuxième exemple

```c
#include<stdio.h>

main()
{
  int number;
  printf( & quot; Type a number: & quot;);
  scanf( & quot; % d & quot;, & amp; number);

  /* check whether the number is negative number */  if (number & lt; 0) {
    /* If it is a negative then convert it into positive. */   
    number = -number;  
    printf( & quot; The absolute value is % d\ n & quot;, number);
  }
  grtch();
} 
```

Et sa sortie:

![c-decision-making-if](https://www.w3schools.in/wp-content/uploads/2014/08/c-decision-making-if.jpg)

## 9.2. if-else 

Sera exécuter si la condition de "if" n'est pas rencontrée (Ne pas confondre avec else-if où on met un "else if" sous un "if")

Syntaxe:

```c
if(test_expression)
{
   //execute your code
}
else
{
   //execute your code
}
```

Exemple 1:

```c
#include<stdio.h>

main()
{
  int a, b;

  printf("Please enter the value for a:");
  scanf("%d", & amp; a);

  printf("\nPlease the value for b:");
  scanf("%d", & amp; b);

  if (a & gt; b) {  
    printf("\n a is greater");
  } else {  
    printf("\n b is greater");
  }
}
```

Sortie 1:

![c-if-else statement 1](https://www.w3schools.in/wp-content/uploads/2014/08/c-if-else-1.jpg)

Exemple 2:

```c
#include<stdio.h>

main() {
  int num;
  printf("Enter the number:");
  scanf("%d", num);

  /* check whether the number is negative number */  if (num < 0)
    printf("The number is negative.");
  else
    printf("The number is positive."); 
}
```

Sortie 2:

![c-if-else-statement 2](https://www.w3schools.in/wp-content/uploads/2014/08/c-if-else-2.jpg)

## 9.3. Nested if-else (un if dans un if)

Syntaxe:

```c
if(test_expression one)
{
   if(test_expression two) {
      //Statement block Executes when the boolean test expression two is true.
   }
}
else
{
    //else statement block
}
```

Exemple:

```c
#include<stdio.h>

main()
{
int x=20,y=30;

    if(x==20)
    {
        if(y==30)
        {
            printf("value of x is 20, and value of y is 30.");
        }
    }
}
```

Sortie:

```
value of x is 20, and value of y is 30.
```

## 9.4. else-if

Pour ajouter un deuxième "if" dans le cas où la condition du premier "if" n'est pas rencontrée

Syntaxe:

```c
if(test_expression)
{
   //execute your code
}
else if(test_expression n)
{
   //execute your code
}
else
{
   //execute your code
}
```

